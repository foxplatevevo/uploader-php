<?php
class Uploader{
    private $token          = null;
    private $path           = null;
    private $constraints    = [
        "maxSize"       => null,
        "extensions"    => null
    ];
    
    public function addConstraint($case, $value){
        switch($case){
            case "maxsize":
                $this->constraints["maxSize"] = $value;
                break;
            case "extensions":
                $this->constraints["extensions"] = $value;
                break;
        }
        
        //If token exists apply changes
        if($this->token){
            $this->saveChanges($this->token);
        }
        
        return $this;
    }
    
    public function generateToken(){
        $length = 10;
        $token = "";

        for($i = 0; $i < $length; $i++){
            $token .= $this->generateChar();
        }
        
        //set Token
        $this->token = $token;
        
        $this->saveChanges($token);
        
        return $token;
    }
    
    public function putFile($token = null){
        if($token){
            $this->setToken($token);
        }
        
        //Create data succeeded
        $r = [
            "success"   => false,
            "message"    => null
        ];
        
        //Verify if Prepare_upload exists
        $prepare = $_POST["PREPARE_UPLOAD"];
        
        if($prepare){
            return $this->prepareUpload();
        }
        
        //Get settings of token
        $settings = $this->getSettings();
        
        if(!$settings["prepared"]){
            $r["message"] = "Ohhh! You want to access without prepare the Upload";
            
            return $r;
        }
        
        $path = $this->cleanPath($settings["path"]);
        
        //Prepare file
        $file = $_FILES["file"];
        
        $basePath       = $_SERVER['DOCUMENT_ROOT'];
        $fileError      = $file["error"];
        $fileName       = $file["name"];
        $fileSize       = $file["size"];
        $fileNameTemp   = $file["tmp_name"];
        $fileType       = $file["type"];
        
        //Create destiny route
        $fileRoute = "/{$path}/{$fileName}";
        $destiny = "{$basePath}{$fileRoute}";
        
        if($fileError === UPLOAD_ERR_OK){
            if(move_uploaded_file($fileNameTemp, $destiny)){
                $r["imagePresentation"] = ($this->isImage($fileName))? $fileRoute : null;
                $r["success"] = true;
            }else{
                $r["message"] = "Destiny directory don't exists or isn't writable";
            }
        }else{
            $r["message"] = "Happened a error whit code {$fileError}";
        }
        
        return $r;
    }
    
    public function setPath($path){
        $this->path = $path;
        
        return $this;
    }
    
    public function setToken($token){
        if($_SESSION[$token]){
            $this->token = $token;
        }else{
            echo "Your token is incorrect or fake";
            exit();
        }
        
        return $this;
    }
    
    private function cleanPath($path){
        $path = str_replace("../", "", $path);
        $path = str_replace("//", "/", $path);
        
        while($path[0] === "/"){
            $array = str_split($path, 1);
            array_shift($array);
            $path = implode("", $array);
        }

        while($path[strlen($path) - 1] === "/"){
            $array = str_split($path, 1);
            array_pop($array);
            $path = implode("", $array);
        }
        
        return $path;
    }
    
    private function generateChar(){
        if(rand(0, 1)){
            //Rango ASCII de numeros
            $randAscii = rand(97,122);
        }else{
            if(rand(0, 1)){
                //Rango ASCII de alfabeto minuscula
                $randAscii = rand(48,57);
            }else{
                //Rango ASCII de alfabeto mayuscula
                $randAscii = rand(65,90);
            }
        }

        return chr($randAscii);
    }
    
    private function getSettings(){
        return $_SESSION[$this->token];
    }
    
    private function isImage($fileName){
        $imageExtension = ["jpg", "png", "gif"];
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        
        return in_array($extension, $imageExtension);
    }
    
    private function prepareUpload(){
        //Create data succeeded
        $r = [
            "success"   => false,
            "message"    => null
        ];
        
        //Get settings of token
        $settings = $this->getSettings();
        
        $constraints = $settings["constraints"];
        //Constraints
        $maxSize = $constraints["maxSize"];
        $extensions = $constraints["extensions"];
        
        //Filter with settings
        $fileName       = $_POST["name"];
        $fileSize       = $_POST["size"];
        
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        
        if(in_array($extension, $extensions) && $fileSize < ($maxSize * 1024 * 1024)){
            $_SESSION[$this->token]["prepared"] = true;
            
            $r["success"] = true;
        }else if(!in_array($extension, $extensions)){
            $extensionStr = implode(", ", $extensions);
            
            $r["message"] = "La extensión del archivo es {$extension}, debe tener las siguientes extensiones {$extensionStr}";
        }else if(!($fileSize < ($maxSize * 1024 * 1024))){
            $r["message"] = "El archivo supera los {$maxSize} MB";
        }
        
        return $r;
    }
    
    //Save token and settings in session vars
    private function saveChanges($token){
        $_SESSION[$token] = [
            "constraints"   => $this->constraints,
            "path"          => $this->path,
            "prepared"      => false
        ];
    }
}