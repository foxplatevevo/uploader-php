<?php
//Disable error reporting
error_reporting(E_ERROR);

//You should init session because Uploader class functions with it
session_start();

require_once('/lib/Uploader.php');

$uploader = new Uploader();

$uploader
->addConstraint("maxsize", 300)
->addConstraint("extensions", ["png", "jpg", "gif"])
->setPath("/test");

$token = $uploader->generateToken();
?>
<html>
    <head>
        <link href="/public/css/style.css" rel="stylesheet">
        <script src="/public/js/jquery-2.1.1.min.js"></script>
        <script src="/public/js/uploader.js"></script>
    </head>
    <body>
        <div id="uploader"></div>
        <script>
            var uploader = new Uploader({
                id      : "#uploader",          //Id of DOM container 
                name    : "inputName",          //Name of input type hidden
                preFiles: ["hola.jpg"],         //Files will show before upload someone file
                token   : "<?= $token ?>",      //Token generated
                type    : Uploader.MULTI_ITEM,  //Or Uploader.MONO_ITEM (One file)
                URI     : "/upload.php"         //URI for request upload
            });
            
            uploader.open();
        </script>
    </body>
</html>