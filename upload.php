<?php
//Disable error reporting
error_reporting(E_ERROR);

//You should init session because Uploader class functions with it
session_start();

require_once('/lib/Uploader.php');

//Receive token to hability upload transfer
$token = $_POST["token"];

$uplaoder = new Uploader();
$uplaoder->setToken($token);

$uploadData = $uplaoder->putFile();

echo json_encode($uploadData);